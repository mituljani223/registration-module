
var page;
	$(document).ready(function() {
		
		for (i = new Date().getFullYear(); i > 2000; i--)
		{
		    $('#yearpicker').append($('<option value='+i+'/>').val(i).html(i));
		}
		
		month = 0;
		var curmonth = (new Date()).getMonth();
		var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		for (; month < monthNames.length; month++) {
			if(month == curmonth){
				$('#monthpicker').append('<option value='+(month+1)+' selected>' + monthNames[month] + '</option>');
			}
			else{
		  		$('#monthpicker').append('<option value='+(month+1)+'>' + monthNames[month] + '</option>');
			}
		}	
		
		$("#footer").load("footer.jsp");
    	$("#filterc").hide();
    	$("#sw").hide();
    	$("#slist").hide();
    	$("#progressbar").hide();
    	$("#includedContent").empty();
		$("#includedContent").load("dashbord.jsp",function(data, status){
			if(status=="success")
				$("#progressbar").hide(500);
		}); 
		$("#lsetting").on("click", function() {
			$("#slist").toggle(500);
		});
		$("#mfiny").on("click", function(){
			$("#includedContent").empty();
			$("#includedContent").load("mfinyear.jsp", //loading page url POST method
    				function(data, status, jqXGR) {  // callback function 
						if(status=="success"){
							$("#progressbar").hide(500);
			        		$("#filterdata").attr("disable",true);
			        		
						}
					}
				);
		});
    	$("#btnse").on("click", function(){
    		if($("#fgpf").val() != ""){
    			var gno = $("#fgpf").val();
    			$("#progressbar").show();
    			if(page=="wd"){
        			$("#includedContent").load("withdraw.jsp", //loading page url POST method
            				{gpfno:gno}, // Perameters
            				function(data, status, jqXGR) {  // callback function 
    							if(status=="success"){
    								$("#progressbar").hide(500);
    				        		$("#filterdata").attr("disable",false);
    				        		$('#sins option').eq(0).prop('selected', true);
    							}
    						}
       				);
        		}
    		}else $("#error").addClass("d-block");
    	});
    	$("#fgpf").on("click", function(){
   			$("#error").removeClass("d-block");
    	} );
    	$("#sins").on('change', function(){
    		$("#progressbar").show();
    		var msins = $('#sins').val();
    		var mmonth = $('#monthpicker').val();
    		var myear = $('#yearpicker').val();
    		$("#includedContent").empty();
    		if(page=="credit"){
        		$("#includedContent").load("creditdebit.jsp", //loading page url POST method
        				{sins:msins, month:mmonth, year:myear}, // Perameters
        				function(data, status, jqXGR) {  // callback function 
							if(status=="success"){
								$("#progressbar").hide(500);
				        		$("#filterdata").attr("disable",false);
				        		$('#sins option').eq(0).prop('selected', true);
							}
						}
   				);
    		}else if(page=="obal"){
    			$("#includedContent").load("openingBalance.jsp", //loading page url POST method
        				{sins:msins}, // Perameters
        				function(data, status, jqXGR) {  // callback function 
							if(status=="success"){
								$("#progressbar").hide(500);
				        		$("#filterdata").attr("disable",false);
				        		$('#sins option').eq(0).prop('selected', true);
							}
						}
   				);
    		}else if(page=="listfac"){
    			$("#includedContent").load("listemp.jsp", //loading page url POST method
        				{sins:msins}, // Perameters
        				function(data, status, jqXGR) {  // callback function 
							if(status=="success"){
								$("#progressbar").hide(500);
				        		$("#filterdata").attr("disable",false);
				        		$('#sins option').eq(0).prop('selected', true);
							}
						}
   				);
    		}
    	});
	});
	
	
	 
	 function showlistemp() {
		 $("#includedContent").empty();
			$('#filterc').show();
			$("#page-title").html("Faculty List");
			$("#monthpicker").hide();
			$("#yearpicker").hide();
			$("#sw").hide();
			page="listfac";
	}
	function showaddemp() {
		$("#progressbar").show();
		$("#filterc").hide();
		$("#sw").hide();
		$("#includedContent").empty();
		$("#includedContent").load("addemp.jsp",function(data, status){
			if(status=="success")
				$("#progressbar").hide(500);
		}); 
	}
	function showdash() {
		  page="dashbord";
		  $("#progressbar").show();
			$("#filterc").hide();
			$("#sw").hide();
			$("#includedContent").empty();
			$("#includedContent").load("dashbord.jsp",function(data, status){
				if(status=="success")
					$("#progressbar").hide(500);
			}); 
	}
	function cd() {
		$("#includedContent").empty();
		$('#filterc').show();
		$("#sw").hide();
		$("#page-title").html("Credit");
		$('#monthpicker').show();
		$('#yearpicker').show();
		page="credit";
	}
	function wd() {
		$("#includedContent").empty();
		$("#filterc").hide();
		$("#page-title").html("Withdraw");
		$("#sw").show();
		page="wd";
	}
	function obal(){
		$("#includedContent").empty();
		$("#monthpicker").hide();
		$("#yearpicker").hide();
		$("#sw").hide();
		$('#filterc').show();
		$("#page-title").html("Opening Balance");
		page="obal";
	}
	function bsheet(){
		$("#includedContent").empty();
		$("progress").show();
		$("#sw").hide();
		$("#includedContent").load("Balancesheet.jsp");
		$("progress").hide();
	}
