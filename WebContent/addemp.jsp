<%@ page import="bean.FACULTY_MASTER"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ page import="bean.FACULTY_MASTER" %>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%-- <%
	 response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");
	     if(session.getAttribute("user")==null){
	     response.sendRedirect("index.jsp");
	
	 }
 %> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" 
	integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

  <link rel="stylesheet" href="bootstrap-4.4.1/css/bootstrap.min.css">
  <script src="bootstrap-4.4.1/js/bootstrap.min.js"></script>
  <script src="bootstrap-4.4.1/jquery341.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  
<title>Add Faculty</title>
</head>
<body>
	<form class="form-group bg-light p-3 shadow" method="post" action="fregistration">
		<div class="modal-header">
          <h4 class="modal-title">Add Faculty</h4>
          <button type="button" class="close" data-dismiss="modal"><small>All * fields are mandatory</small></button>
        </div>
            <div class="row mb-3 mt-3">
           
                <div class="col pad">
                	<div class="input-group">
					    <div class="input-group-prepend">
					      <span class="input-group-text text-danger font-weight-bolder">*</span>
					    </div>
                 <!-- <label for="message-text" class="col-form-label">Institute:</label>  -->
                    <select class="form-control" name="inst" required>
                    <option selected>Choose Institute</option>
                   	<option value="1">Government MCA College, Maninagar</option>
                   	<option value="2">L.D. College of Engineering</option>
                    </select>
                </div>
                </div>
              </div>
              <div class="row">
                <div class="col pad">
	                <div class="input-group mb-3">
					    <div class="input-group-prepend">
					      <span class="input-group-text text-danger font-weight-bolder">*</span>
					    </div>
	                 <!--  <label for="message-text" class="col-form-label">Surname:</label> -->
	                  <input type="name" name="surname" class="form-control" pattern="[A-Za-z]+"
	                 placeholder="Surname" required>
	                 </div>
                </div>
                <div class="col pad">
                	<div class="input-group mb-3">
					    <div class="input-group-prepend">
					      <span class="input-group-text text-danger font-weight-bolder">*</span>
					    </div>
                 <!-- <label for="message-text" class="col-form-label">Firstname:</label> -->
                <input type="text" class="form-control" name="fname" pattern="[A-Za-z]+" id="recipient-name" placeholder="Firstname" required>
                </div>
                </div>
                <div class="col pad">
                	<div class="input-group mb-3">
					    <div class="input-group-prepend">
					      <span class="input-group-text text-danger font-weight-bolder">*</span>
					    </div>
                  <!-- <label for="message-text" class="col-form-label">Middlename:</label> --> 
                <input type="text" class="form-control" id="recipient-name" pattern="[A-Za-z]+" name="sname" placeholder="Lastname" required>
                	</div>
                </div> 
              </div>
              <div class="row">
                <div class="col pad">
                	<div class="input-group mb-3">
					    <div class="input-group-prepend">
					      <span class="input-group-text text-danger font-weight-bolder">*</span>
					    </div>
                	 <!-- <label for="message-text" class="col-form-label">Contact:</label> --> 
                <input type="number" maxlength="10" class="form-control" pattern="[0-9]{10}" placeholder="Mobile No." name="mno" required>
                </div>
                </div>
                <div class="col pad">
                <div class="input-group mb-3">
					    <div class="input-group-prepend">
					      <span class="input-group-text text-danger font-weight-bolder">*</span>
					    </div>
                  <!-- <label for="message-text" class="col-form-label">Email:</label> -->
                <input type="email" class="form-control" placeholder="Email" name="email" required>
                </div>    
                </div>
            </div>    
            <div class="row">
                <div class="col pad">
                <div class="input-group mb-3">
					    <div class="input-group-prepend">
					      <span class="input-group-text text-danger font-weight-bolder">*</span>
					      <span class="input-group-text">Birthdate</span>
					    </div>
                  <!-- <label for="message-text" class="col-form-label">Birthdate:</label> -->
                <input type="date" class="form-control" id="recipient-name" name="dob" required />
                </div>
                </div>
                <div class="col pad">
                  <!-- <label for="message-text" class="col-form-label">Gender:</label></br> -->
	                  <div class="input-group mb-3 border">
						    <div class="input-group-prepend">
						      <span class="input-group-text text-danger font-weight-bolder">*</span>
						      <span class="input-group-text">Gender</span>
						    </div>
						    
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input ml-2" type="radio" name="gen" id="inlineRadio" value="m" checked>
			                    <label class="form-check-label" for="radiogen">Male</label>
			                </div>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="gen" id="inlineRadio2" value="f">
			                    <label class="form-check-label" for="inlineRadio2">Female</label>
			                </div>
				           	
	                </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col pad">
                <!-- <label for="message-text" class="col-form-label">Retirement Date:</label> -->
	                <div class="input-group">
					    <div class="input-group-prepend">
					    	<span class="input-group-text text-danger font-weight-bolder">*</span>
					      <span class="input-group-text">Retirement Date</span>
					    </div>
	                	<input type="date" class="form-control" name="rdate" required>
                	</div>
                </div>
                <div class="col pad">
                  <!-- <label for="message-text" class="col-form-label">Designation:</label> -->
                  <div class="input-group">
					    <div class="input-group-prepend">
					    	<span class="input-group-text text-danger font-weight-bolder">*</span>
					    </div>
                    <select class="form-control" id="" name="desgid" required>
                      <option value="1" selected>Choose Designation</option>
                      <option value="2">Ast. Proffesor</option>
                      <option value="3">Lacturer</option>
                      <option value="4">LAB Ast.</option>
                    </select>
                </div>
                </div>
              </div>

              <div class="row mb-3">
                <div class="col pad">
                	<div class="input-group">
					    <div class="input-group-prepend">
					      <span class="input-group-text text-danger font-weight-bolder">*</span>
					    </div>
		                  <!-- <label for="message-text" class="col-form-label">Bank Name:</label> -->
		                <input type="text" class="form-control" name="bname" placeholder="Bank Name" required>
	                </div>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col pad">
                	<div class="input-group">
					    <div class="input-group-prepend">
					      <span class="input-group-text text-danger font-weight-bolder">*</span>
					    </div>
		                  <!-- <label for="message-text" class="col-form-label">Branch Name:</label> -->
		                <input type="text" class="form-control" placeholder="Branch Name" name="brname" required>
		            </div>
                </div>
                <div class="col pad">
                	<div class="input-group">
					    <div class="input-group-prepend">
					      <span class="input-group-text text-danger font-weight-bolder">*</span>
					    </div>
	                  	<!-- <label for="message-text" class="col-form-label">Account Number:</label> -->
	                	<input type="text" class="form-control" maxlength="10" pattern="[0-9]+" placeholder="Account Number" name="bano" required>
	                </div>
                </div>
            </div>
            <div class="row mb-3">
                
                <div class="col pad">
                	<div class="input-group">
					    <div class="input-group-prepend">
					      <span class="input-group-text text-danger font-weight-bolder">*</span>
					    </div>
		                  <!-- <label for="message-text" class="col-form-label">IFSC Code:</label> -->
		                <input type="text" class="form-control" placeholder="IFSC Code" name="ifscc" required>
		             </div>
                </div>
                <div class="col pad">
                <div class="input-group">
					    <div class="input-group-prepend">
					      <span class="input-group-text text-danger font-weight-bolder">*</span>
					    </div>
                 <!--  <label for="message-text" class="col-form-label">GPF Number:</label> -->
                		<input type="text" class="form-control" placeholder="GPF Number" pattern="[0-9]+" name="gpfno" required>
                	</div>
                </div>

            </div>
            <div class="modal-footer">
              <input type="submit" class="btn btn-success mx-auto btn-block" name="submit" value="Submit">
            </div>
   </form>
</body>
</html>