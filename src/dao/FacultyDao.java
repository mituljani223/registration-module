package dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import bean.FACULTY_MASTER;

import java.sql.Timestamp;


public class FacultyDao {
	static int fid;
	static SessionFactory factory;
	static Session session = getConnection();
	
	public static Session getConnection() {
		Configuration cn=new Configuration();
		cn.configure();
		StandardServiceRegistry servicereg=new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
	    factory = cn.buildSessionFactory(servicereg);
	    Session ession = factory.getCurrentSession();
	    ession.beginTransaction();
	    return ession;
	}
	
	public void addFaculty(String surname, String fname, 
			String sname, String email, String mno, String gpfno,
			String bano, String bname, String brname, String ifscc, 
			String remarks, char gen, String dob, String rdate,
			int insname, int desgid, int curid) {
		
			try {
				
				// 4. Starting Transaction
				Transaction transaction = session.beginTransaction();
				FACULTY_MASTER f = new FACULTY_MASTER();
				f.setFname(fname);f.setSurname(surname);f.setSname(sname);
				f.setEmail(email);f.setMno(mno);f.setGpfno(gpfno);f.setBano(bano);
				f.setBname(bname);f.setBrname(brname);f.setIfscc(ifscc);
				f.setRemarks(remarks);f.setGen(gen);
				f.setDob(new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(dob)); 
				f.setRdate(new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(rdate));
				f.setInsname(insname);f.setDesgid(desgid);
				session.save(f);
				transaction.commit();
				System.out.println("\n\n Details Added \n");
			} catch (HibernateException | ParseException e) {
				System.out.println(e.getMessage());
				System.out.println("error");
			} 
		
	}


	/*public Iterator<FACULTY_MASTER> getEmp(String q){
	    List list = session.createQuery(q).list();
	    Iterator it = list.iterator();
	    session.clear();
		return it;
	}
	public List<FACULTY_MASTER> getEmplist(String q){
	    List list = session.createQuery(q).list();
	    session.clear();
		return list;
	}*/
	
	public Timestamp getTs() {
		Date date = new Date();
		long time = date.getTime();
		Timestamp ts = new Timestamp(time);
		return ts;
	}
	
}
