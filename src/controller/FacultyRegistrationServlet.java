package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mchange.v2.cfg.PropertiesConfigSource.Parse;

import bean.FACULTY_MASTER;
import dao.FacultyDao;

public class FacultyRegistrationServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		System.out.println("in dopost");
		try {		
			
			FacultyDao fDAO = new FacultyDao();
			String surname = req.getParameter("surname");
			String fname = req.getParameter("fname");
			String sname = req.getParameter("sname");
			String mno = req.getParameter("mno");
			String email = req.getParameter("email");
			String dob = req.getParameter("dob");
			String gen = req.getParameter("gen");
			String rdate = req.getParameter("rdate");
			String desgid = req.getParameter("desgid");
			
			String bname = req.getParameter("bname");
			String brname = req.getParameter("brname");
			String bano = req.getParameter("bano");
			String ifscc = req.getParameter("ifscc");
			String gpfno = req.getParameter("gpfno");
			String inst = req.getParameter("inst");
			
			System.out.println(surname);
			System.out.println(fname);
			System.out.println(sname);
			System.out.println(mno);
			System.out.println(email);
			System.out.println(dob);
			System.out.println(gen);
			
			System.out.println(rdate);
			System.out.println(desgid);
			System.out.println(bano);
			System.out.println(bname);
			System.out.println(ifscc);
			System.out.println(gpfno);
			System.out.println(inst);
			System.out.println(brname);
			char g = gen.charAt(0);
			int ins = Integer.parseInt(inst);
			int desg = Integer.parseInt(desgid);
			fDAO.addFaculty(surname, fname, sname, email,
					mno, gpfno, bano, 
					bname, brname, ifscc, "fdh", g, dob, rdate, ins,desg, 1);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
}
