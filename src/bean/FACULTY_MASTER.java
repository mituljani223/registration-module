package bean;

import java.util.Date;

public class FACULTY_MASTER {

	private int id;
	private String surname, fname, sname, email,
					mno, gpfno, bano, bname, brname,
					ifscc, remarks, password;
	private char gen;
	
	private Date dob,rdate;
	private int insname,desgid;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMno() {
		return mno;
	}
	public void setMno(String mno) {
		this.mno = mno;
	}
	public String getGpfno() {
		return gpfno;
	}
	public void setGpfno(String gpfno) {
		this.gpfno = gpfno;
	}
	public String getBano() {
		return bano;
	}
	public void setBano(String bano) {
		this.bano = bano;
	}
	public String getBname() {
		return bname;
	}
	public void setBname(String bname) {
		this.bname = bname;
	}
	public String getBrname() {
		return brname;
	}
	public void setBrname(String brname) {
		this.brname = brname;
	}
	public String getIfscc() {
		return ifscc;
	}
	public void setIfscc(String ifscc) {
		this.ifscc = ifscc;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public char getGen() {
		return gen;
	}
	public void setGen(char gen) {
		this.gen = gen;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public Date getRdate() {
		return rdate;
	}
	public void setRdate(Date rdate) {
		this.rdate = rdate;
	}
	public int getInsname() {
		return insname;
	}
	public void setInsname(int insname) {
		this.insname = insname;
	}
	public int getDesgid() {
		return desgid;
	}
	public void setDesgid(int desgid) {
		this.desgid = desgid;
	}
}
